mod widgets;
mod app;

use snui::wayland::shell::*;
use snui::widgets::container::*;
use snui::{*, widgets::{*, text::*}};
use smithay_client_toolkit::reexports::calloop::{
    EventLoop,
    LoopHandle,
    RegistrationToken,
    timer::Timer
};
use num_enum::{IntoPrimitive, TryFromPrimitive};

#[derive(IntoPrimitive, TryFromPrimitive, Debug, Clone, PartialEq, Eq, Copy)]
#[repr(u32)]
pub enum Request {
    Tag,
    TopLevel,
    Title,
    Time,
    Other,
    Volume,
    Many,
}

fn main() {
    let (mut snui, mut event_loop) = Application::new(true);

	for output in snui.globals.get_outputs() {
        snui.create_inner_application(
            app::Bar::default(),
            bar()
            .ext()
            .background(style::BG0)
            .border(style::BG2, 1.)
            .padding(5., 5., 5., 5.)
            .with_width(output.width as f32),
            event_loop.handle(),
            |_, _| {},
        );
	}

	let handle = event_loop.handle();

	let timer = Timer::new().unwrap();
	let timer_handle = timer.handle();
	timer_handle.add_timeout(
    	std::time::Duration::from_secs(60),
    	Event::Message(
        	data::Message::new(Request::Time as u32, 10)
    	)
	);

	handle.insert_source(
    	timer_handle,
    	move |ev, _, application| {
        	if let Request::Time = ev {
            	for inner_application in application.inner.iter_mut() {
                	inner_application.callback(Event::Message(
                    	data::Message::new(Request::Time as u32, 10)
                	));
                	// inner_application.controller.
            	}
        	}
    	}
	).unwrap();

    snui.run(&mut event_loop);
}

fn bar() -> impl Widget {
    CenterBox::from(
        tagbar(),
        Listener::new(Label::default("title", 16.))
        .id(Request::Title as u32),
        system()
    )
}

fn tagbar() -> WidgetLayout {
    (0..10).map(|tag| {
        widgets::Tag::new((1 << tag) / 2)
        .child()
    })
    .collect::<WidgetLayout>()
    .spacing(10.)
}

fn system() -> impl Widget {
    let mut system = WidgetLayout::new(10.);
    system.add(
        slider::Slider::new(120, 8)
        .id(Request::Volume as u32)
        .background(style::ORG)
        .ext().background(style::BG1)
        .even_radius(3.)
    );
    system.add(
        shapes::Rectangle::empty(1., 15.)
        .background(style::BG1)
    );
    system.add(
        Listener::new(Label::default("12:00", 16.))
        .id(Request::Time as u32)
    );
    system.justify(CENTER);
    system
}
