use snui::data::*;
use super::Request;
use super::widgets::State;
// use wayland_protocols::wlr::unstable::foreign_toplevel::v1::client::{
//     zwlr_foreign_toplevel_handle_v1::*,
//     zwlr_foreign_toplevel_manager_v1::*
// };
use std::time::Duration;
use std::path::{Path, PathBuf};
use std::collections::HashMap;
use notify::{Watcher, RecursiveMode, watcher, DebouncedEvent};

#[derive(Debug, Clone)]
struct River {
    title: String,
    tags: u32,
    views_tag: Vec<u32>
}

#[derive(Debug, Clone)]
pub struct Bar {
    sessions: HashMap<PathBuf, State>,
    river: River,
}

impl Default for Bar {
    fn default() -> Self {
        Bar {
            sessions: HashMap::new(),
            river: River {
                title: String::from("app title"),
                tags: 0,
                views_tag: Vec::new()
            }
        }
    }
}

impl Controller for Bar {
    fn serialize(&mut self, _msg: Message) -> Result<u32, ControllerError> {
        Err(ControllerError::WrongObject)
    }
    fn deserialize(&mut self, _token: u32) -> Result<(), ControllerError> {
        Err(ControllerError::WrongObject)
    }
    fn get<'c>(&'c self, msg: Message) -> Result<Data<'c>, ControllerError> {
        let Message(obj, data) = msg;
        if let Ok(request) = Request::try_from(obj) {
            match request {
                Request::Title => return Ok(self.river.title.as_str().into()),
                Request::Tag => match data {
                    Data::Uint(tag) => {
                        if tag & self.river.tags == tag {
                            return Ok(Data::Any(&State::Focused));
                        } else if self.river.views_tag.binary_search(&tag).is_ok() {
                            return Ok(Data::Any(&State::Occupied));
                        }
                    }
                    _ => {}
                }
                _ => {}
            }
        }
        Err(ControllerError::WrongObject)
    }
    fn send<'c>(&'c mut self, msg: Message) -> Result<Data<'c>, ControllerError> {
        let Message(obj, data) = msg;
        if let Ok(request) = Request::try_from(obj) {
            match request {
                Request::Title => self.river.title = data.to_string(),
                Request::Other => {}
                _ => {}
            }
        }
        Ok(Data::Null)
    }
    fn sync(&mut self) -> Result<Message<'static>, ControllerError> {
        Err(ControllerError::NonBlocking)
    }
}
