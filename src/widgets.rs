use super::{Request};

use snui::data::*;
use scene::Instruction;
use snui::widgets::shapes::*;
use snui::{*, widgets::{text::*, *}};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum State {
    Occupied,
    Focused,
    Empty,
}

pub struct Tag {
    tag: u32,
    state: State,
    size: f32
}

impl Geometry for Tag {
    fn height(&self) -> f32 {
        self.size
    }
    fn width(&self) -> f32 {
        self.size
    }
}

impl Widget for Tag {
    fn create_node(&mut self, x: f32, y: f32) -> scene::RenderNode {
        Instruction::new(
            x, y,
            Rectangle::empty(self.width(), self.height())
            .background(match self.state {
                State::Empty => style::BG1,
                State::Focused => style::YEL,
                State::Occupied => style::GRN,
            })
            .even_radius(3.)
        ).into()
    }
    fn sync<'d>(&'d mut self, ctx: &mut context::SyncContext, event: Event) -> Damage {
        match event {
            Event::Pointer(x, y, p) => if self.contains(x, y) {
                match p {
                    Pointer::MouseClick { time:_, button, pressed } => {
                        if pressed && button.is_left() {
                            todo!();
                        }
                    }
                    _ => {} }
            }
            Event::Message(msg) => {
                let ev_type = Request::Tag;
                let Message(obj, _) = msg;
                if obj == ev_type as u32 {
                    if let Ok(data) = ctx.get(Message::new(Request::Tag as u32, self.tag)) {
                        match data {
                            Data::Any(state) => if let Some(state) = state.downcast_ref::<State>() {
                                if self.state.ne(state) {
                                    self.state = *state;
                                    return Damage::Some;
                                } else {
                                    return Damage::None;
                                }
                            }
                            _ => {}
                        }
                    }
                }
            }
            Event::Frame => {
                if let Ok(data) = ctx.get(Message::new(Request::Tag as u32, self.tag)) {
                    match data {
                        Data::Any(state) => if let Some(state) = state.downcast_ref::<State>() {
                            if self.state.ne(state) {
                                self.state = *state;
                                return Damage::Some;
                            } else {
                                return Damage::None;
                            }
                        }
                        _ => {}
                    }
                }
            }
            _ => {}
        }
        Damage::None
    }
}

impl Tag {
    pub fn new(tagmask: u32) -> Tag {
        Tag {
            state: State::Empty,
            tag: tagmask,
            size: 10.
        }
    }
}

pub struct Session {
    pub state: State,
    pub name: WidgetExt<WidgetBox<Text>>,
}

impl Geometry for Session {
    fn width(&self) -> f32 {
        self.name.width()
    }
    fn height(&self) -> f32 {
        self.name.height()
    }
}

impl Widget for Session {
    fn create_node(&mut self, x: f32, y: f32) -> scene::RenderNode {
        match self.state {
            State::Empty => {
                self.name.set_color(style::BG2);
                self.name.set_border_color(style::BG1);
                self.name.set_background(style::BG0);
            }
            State::Focused => {
                self.name.set_color(style::YEL);
                self.name.set_border_color(style::YEL);
                self.name.set_background(style::BG1);
            }
            State::Occupied => {
                self.name.set_color(style::FG0);
                self.name.set_border_color(style::BG1);
                self.name.set_background(style::BG1);
            }
        }
        self.name.create_node(x, y)
    }
    fn sync<'d>(&'d mut self, ctx: &mut context::SyncContext, event: Event) -> Damage {
        match event {
            Event::Pointer(x, y, p) => if self.contains(x, y) {
                match p {
                    _ => {
                        todo!()
                    }
                }
            }
            Event::Message(msg) => {
                let Message(obj, _) = msg;
                if let Ok(data) = ctx.get(Message::new(obj, self.name.get_text())) {
                    match data {
                        Data::Any(state) => if let Some(state) = state.downcast_ref::<State>() {
                            if self.state.ne(state) {
                                self.state = *state;
                                return Damage::Some;
                            }
                        }
                        Data::Boolean(active) => {
                            if active {
                                self.state = State::Focused;
                                return Damage::Some;
                            }
                        }
                        _ => {}
                    }
                }
            }
            _ => {}
        }
        Damage::None
    }
}
